/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.service;
import com.pattraporn.onesoftwarecorporation.dao.UserDao;
import com.pattraporn.onesoftwarecorporation.model.User;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author werapan
 */
public class UserService {
    private static ArrayList<User> userlist = new ArrayList<>();

    static {
        userlist.add(new User("admin", "password"));
        userlist.add(new User("user1", "password"));

    }

    public static boolean addUser(User user) {
        userlist.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userlist.add(new User(userName, password));
        return true;
    }
    public static User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if(user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
    public static User loginlist(String userName, String password) {
        for (User user : userlist) {
            if (user.getName().equals(userName) && user.getPassword().equals(password));
            return user;
        }
        return null;
    }
  public List<User>getUsers(){
      UserDao userDao = new UserDao();
      return userDao.getAll(" user_login asc");
  }

    public User addNew(User edtedUser) {
         UserDao userDao = new UserDao();
         return userDao.save(edtedUser);
    }

    public User update(User edtedUser) {
          UserDao userDao = new UserDao();
         return userDao.update(edtedUser);
    }

    public int delete(User edtedUser) {
       UserDao userDao = new UserDao();
         return userDao.delete(edtedUser);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.dao;


import com.pattraporn.onesoftwarecorporation.helper.DatabaseHelper;
import com.pattraporn.onesoftwarecorporation.model.Orders_Detail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class Orders_DetailDao implements Dao<Orders_Detail> {

    @Override
    public Orders_Detail get(int id) {
        Orders_Detail item = null;
        String sql = "SELECT * FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Orders_Detail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<Orders_Detail> getAll() {
        ArrayList<Orders_Detail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders_Detail item = Orders_Detail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
      
    @Override
    public List<Orders_Detail> getAll(String where, String order) {
        ArrayList<Orders_Detail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders_Detail item = Orders_Detail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
         public List<Orders_Detail> getByOrdersId(int orderId) {
       return getAll("order_id = " + orderId, "order_detail_id ASC");
    }

    public List<Orders_Detail> getAll(String order) {
        ArrayList<Orders_Detail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders_Detail item = Orders_Detail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders_Detail save(Orders_Detail obj) {

        String sql = "INSERT INTO order_detail (product_id,qty,product_price,product_name,order_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getproduct().getId());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getproductPrice());
            stmt.setString(4, obj.getproductName());
            stmt.setInt(5, obj.getOrder().getId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Orders_Detail update(Orders_Detail obj) {
//        String sql = "UPDATE item"
//                + " SET item_login = ?, item_name = ?, item_gender = ?, item_password = ?, item_role = ?"
//                + " WHERE item_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }
    @Override
    public int delete(Orders_Detail obj) {
        String sql = "DELETE FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}

